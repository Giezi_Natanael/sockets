#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
 
int main(int argc, char *argv[])
{
	struct sockaddr_in server;
	char ip_add[16];
	int fd, numbytes,PORT;
	char buf[2048];
	char buf_mess_send[2048];
	system("clear");
	strcpy(ip_add,argv[1]);
	PORT = atoi(argv[2]);
	if ((fd=socket(AF_INET, SOCK_STREAM, 0))==-1){
		printf("socket() error\n");
		exit(-1);
	}
	server.sin_family = AF_INET;
	server.sin_port = htons(PORT);

	server.sin_addr.s_addr=inet_addr(ip_add);
	bzero(&(server.sin_zero),8);
	if(connect(fd, (struct sockaddr *)&server,
	sizeof(struct sockaddr))==-1){
		printf("connect() error\n");
		exit(-1);
	}
	printf("\t\t\t--------> Conexion Establecida por [ %s:%d ]<--------\n",ip_add,PORT);
	if ((numbytes=recv(fd,buf,2048,0)) == -1){
		printf("Error en recv() \n");
		exit(-1);
	}
	printf("\n\t\t\t\t[ %s ]\n\n",buf);

	strcpy(buf_mess_send,"https://www.youtube.com/channel/UCAbAudSaz8kOT_T571YHpJA");//CADENA A ENVIAR A SERVIDOR
	int leng_buf_mess_send = strlen(buf_mess_send);
	int size_buf_mess_send = sizeof(buf_mess_send);
	printf("\t--------->//MENSAJE A ENVIAR [ %s ]\n\t\tLongitud: [ %d ]\n\t\tBytes [ %d ]\n",buf_mess_send,leng_buf_mess_send,size_buf_mess_send); 
	send(fd,buf_mess_send,2048,0);

	recv(fd,buf,2048,0);
	printf("----->Mensaje de Servidor: [ %s ]<-----\n",buf);
	printf("--------------------------CONEXION FINALIZADA--------------------------\n");
	close(fd);
}